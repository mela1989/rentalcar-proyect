import React, { Component } from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import PropTypes from 'prop-types';
import axios from 'axios';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import {Link} from 'react-router-dom';
//import { MdCenterFocusStrong } from 'react-icons/md';
import { CircularProgress } from '@material-ui/core';

const styles = {
    form:{
        textAlign:'center',
        marginTop:'10rem'
    },
    title:{
        textAlign:'center'
    },
    textField: {
        margin: '10px auto 10px auto',
        backgrounColor:'#000'
    },
    buttonForm:{
        marginTop: 20,
        position:'relative'
    },
    customError:{
        color: 'red',
        fontSize: '0.8rem'
    },
    progress:{
        position:'absolute'
    }
}

class login extends Component {
    constructor(){
        super();
        this.state = {
            email: '',
            password: '',
            loadingUser:false,
            errors: {}
        };
    }

    handleSubmit= (event) => {
        event.preventdefault();
        this.setState({
            loadingUser: true
        });
        const userData = {
            email: this.state.email,
            password: this.state.password
        };
        axios
            .post('/login', userData)
            .then((res) => {
            console.log(res.data);
            this.setState({
                loadingUser: false
            });
         
            this.props.history.push('/');
        })
        .catch((err) => {
            this.setState({
                errors: err.response.data,
                loadingUser:false
            });
        });
    };
    handleChange= (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };
    render() {
        const {classes} = this.props;
        const {errors, loadingUser } = this.state;

        return (
            <Grid container className={classes.form}>
            <Grid item sm />
            <Grid item sm>
              <Typography variant="h2" className={classes.pageTitle}>
                Login
              </Typography>
              <form noValidate onSubmit={this.handleSubmit}>
                <TextField
                  id="email"
                  name="email"
                  type="email"
                  label="Email"
                  className={classes.textField}
                  helperText={errors.email}
                  error={errors.email ? true : false}
                  value={this.state.email}
                  onChange={this.handleChange}
                  fullWidth
                />
                <TextField
                  id="password"
                  name="password"
                  type="password"
                  label="Password"
                  className={classes.textField}
                  helperText={errors.password}
                  error={errors.password ? true : false}
                  value={this.state.password}
                  onChange={this.handleChange}
                  fullWidth
                />
                {errors.general && (
                  <Typography variant="body2" className={classes.customError}>
                    {errors.general}
                  </Typography>
                )}
                <Button
                  type="submit"
                  variant="contained"
                  color="primary"
                  className={classes.button}
                  disabled={loadingUser}
                >
                  Login
                  {loadingUser && <CircularProgress size={30} className={classes.progress} />}
                </Button>
                <br />
                <small>
                  dont have an account ? sign up <Link to="/signup">here</Link>
                </small>
              </form>
            </Grid>
            <Grid item sm />
          </Grid>
                   
                   
         );
    }
}

login.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(styles)(login);


