import React from 'react';
import './App.css';
import { Switch, Route } from "react-router-dom";

//pages
import Home from './pages/Home';
import Cars from './pages/Cars';
import SingleCar from './pages/SingleCar';
import Error from './pages/Error';
import login from './pages/login';
import Signup from './pages/Signup';

//components
import Navbar from './components/Navbar';

function App() {
  return (
      <>
      <Navbar />
        <Switch>
          <Route exact path='/' component={Home} />
          <Route exact path='/cars/' component={Cars} />
          <Route exact path='/cars/:slug' component={SingleCar} />
          <Route exact path='/login/' component={login} />
          <Route exact path='/signup/' component={Signup} />
          <Route component={Error} />
        </Switch>
     
     </>
  )
}

export default App;
