import React, { Component } from 'react';
import {IoIosAirplane, IoIosHome} from 'react-icons/io';
import {MdStore} from 'react-icons/md';
import Title from './Titel';

export default class Services extends Component {
    state = {
        services:[
            {
                icon:<IoIosAirplane size={40} />,
                title: 'Desde el Aeropuerto',
                info: 'Lorem ipsum dolor sit amet consectetur adipiscing elit feugiat, nullam pulvinar'
            },
            {
                icon:<IoIosHome size={40}/>,
                title: 'Desde tu casa',
                info: 'Lorem ipsum dolor sit amet consectetur adipiscing elit feugiat, nullam pulvinar'
            },
            {
                icon:<MdStore size={40}/>,
                title: 'Desde nuestra tienda',
                info: 'Lorem ipsum dolor sit amet consectetur adipiscing elit feugiat, nullam pulvinar'
            }
        ]
    }
    render() {
        return (
            <section className='services'>
               <Title title='Nuestros Servicos' />
               <div className='services-center'>
                    {this.state.services.map((item, i) => {
                        return (
                            <article key={i} className='services'>
                                <span>{item.icon}</span>
                                <h6>{item.title}</h6>
                                <p>{item.info}</p>
                            </article>
                        );
                    })}
               </div>
           </section>

        );  
    }
}
