import React from 'react'
import CargandoGif from '../images/gift/cargando.gif';

export default function Loading() {
    return (
        <div className='loading'>
            <h4>Cargando carros...</h4>
            <img src={CargandoGif} className='loadingGif' alt='cargando' />
        </div>
    )
}
