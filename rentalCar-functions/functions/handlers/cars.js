const { db }= require('../util/admin');

exports.getAllCars =  (req, res) =>{
    db
    .collection('cars')
    .orderBy('createdAt', 'desc')
    .get()
    .then((data) => {
        let cars = [];
        data.forEach((doc) => {
            cars.push({
                carId: doc.id,
                body: doc.data().body,
                title: doc.data().title,
                color: doc.data().color,
                createdAt: doc.data().createdAt,
                
            });
         });
        return res.json(cars);
    })
    .catch((err) => {
        console.error(err);
        res.status(500).json({ error: err.code });
      });
  };


exports.postOneCar = (req, res) => {
    if(req.body.body.trim() === '') {
        return res.status(400).json({ body: 'El body no puede estar vacio' });
    }
    const newScream = {
        body: req.body.body,
        title: req.body.title,
        createdAt: new Date().toISOString()
    };
  
    db.collection('cars')
    .add(newScream)
    .then((doc)=> {
        res.json({ message: `document ${doc.id} se creo con exito`});
        
    })
    .catch((err) => {
        res.status(500).json({ error: 'algo salio mal'});
         console.error(err);
    });
};
